package com.example;

/**
 * トランプを表すクラス
 */
public class Card {

    /**
     * カードのマークを表す
     *
     */
    private int mark;

    /**
     * カードの数字を表す
     */
    private int number;

    public Card(int mark, int number) {
        this.mark = mark;
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
