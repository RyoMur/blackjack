package com.example;

import java.util.ArrayList;
import java.util.List;

/**
 * ゲームを遊ぶプレーヤーを表すクラス
 */
public class Challenger implements Player{

    /**
     * 手札を表す
     */
    private List<Card> hands;

    /**
     * 勝負するかどうかを表す
     */
    private boolean isStand = false;

    /**
     * 手札の合計を表す
     */
    private int totalScore = 0;

    public Challenger() {
        this.hands = new ArrayList<Card>();

    }

    /**
     * カードを１枚引いて手札に加えるメソッド
     * 引いたカードの数字をスコアに合算する
     *
     * @param card デッキから引いたカード
     */
    public void hit(Card card) {
        this.hands.add(card);
        this.totalScore += card.getNumber();
    }

    /**
     * プレーヤーがスタンドしたかどうかの値を返すメソッド
     *
     * @return スタンドしてたらtrue,スタンドしてなかったらfalse
     */
    public boolean getIsStand() {
        return isStand;
    }

    /**
     * スタンドするときに使用するメソッド
     * このときにAの計算を行う
     */
    public void stand() {
        for (int i = 0; i < hands.size(); i++) {
            if (hands.get(i).getNumber() == 1) {
                if (this.hands.get(i).getNumber() + 10 <= 21) {
                    this.hands.get(i).setNumber(11);
                    this.totalScore += 10;
                }
            }
        }
        this.isStand = true;
    }

    public List<Card> showHands() {
        return hands;
    }

    public int getTotalScore() {
        return totalScore;
    }
}
