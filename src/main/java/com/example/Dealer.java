package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ディーラーを表すクラス
 */
public class Dealer {

    /**
     * 山札を表す
     */
    private List<Card> deck;

    /**
     * 手札を表す
     */
    private List<Card> hands;

    /**
     * 勝負するかどうかを表す
     */
    private boolean isStand = false;

    /**
     * 手札の合計を表す
     */
    private int totalScore = 0;

    public Dealer() {
        this.deck = new ArrayList<Card>();
        this.hands = new ArrayList<Card>();

        for (int i = 1; i <= 4; i++) {
            for (int j = 1; j <= 13; j++) {
                //絵札はすべて１０として扱う
                if (j > 10) {
                    this.deck.add(new Card(i, 10));
                } else {
                    this.deck.add(new Card(i, j));
                }
            }
        }

        Collections.shuffle(deck);
    }

    /**
     *
     * @return
     */
    public Card releaseCard() {
        Card card = deck.get(0);
        deck.remove(0);
        return card;
    }

    public void hit() {
        hands.add(releaseCard());
        this.totalScore += releaseCard().getNumber();
    }

    public boolean getIsStand() {
        return isStand;
    }

    public void stand() {
        isStand = true;
    }

    public List<Card> getHands() {
        return hands;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void toDecideAction() {
        //ディーラーは手札の合計が17以下だとカードを引かなければならないルール
        if (this.totalScore <= 17) {
            hit();
        } else {
            stand();
        }
    }
}
