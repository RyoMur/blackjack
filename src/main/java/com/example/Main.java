package com.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Challenger player = new Challenger();
        Dealer dealer = new Dealer();
        Output output = new Output();
        //プレーヤーのアクションを標準入力で受け付けるため
        Scanner scanner = new Scanner(System.in);

        output.printStartGame();

        //プレーヤーの手札にカードを２枚追加する
        //ディーラーの手札にカードを２枚追加する
        for (int i = 0; i < 2; i++) {
            player.hit(dealer.releaseCard());
            dealer.hit();
        }

        do {
            System.out.print("あなたの手札|");
            for (int i = 0; i < player.showHands().size(); i++) {
                System.out.print(" " + player.showHands().get(i).getNumber() + " |");
            }
            System.out.println();
            System.out.println("合計：" + player.getTotalScore());

            System.out.print("ディーラーの手札| " + dealer.getHands().get(0).getNumber() + " |");
            for (int i = 1; i < dealer.getHands().size(); i++) {
                System.out.print(" * |");
            }

            System.out.println();
            if (!player.getIsStand()) {
                output.askHit();
                output.yesOrNo();
                int actionNumber = scanner.nextInt();
                if (actionNumber == 0) {
                    player.hit(dealer.releaseCard());
                } else {
                    player.stand();
                }
            }

            dealer.toDecideAction();

        } while (!player.getIsStand() || !dealer.getIsStand());

        System.out.println("あなたのスコア：" + player.getTotalScore());
        System.out.println("ディーラーのスコア：" + dealer.getTotalScore());

        if (player.getTotalScore() <= 21) {
            if (dealer.getTotalScore() <= 21) {
                if (player.getTotalScore() > dealer.getTotalScore()) {
                    output.printYouWin();
                } else if (player.getTotalScore() == dealer.getTotalScore()) {
                    output.printDraw();
                } else {
                    output.printYouLose();
                }
            } else {
                output.printYouWin();
            }
        } else {
            output.printYouLose();
        }

        output.printEndGame();
    }
}
