package com.example;

/**
 * 出力をまとめたクラス
 */
public class Output {

    public void printEndGame() {
        System.out.println("ゲームを終了します");
    }

    public void printStartGame() {
        System.out.println("=====ブラックジャックゲーム===");
    }

    public void printYouWin() {
        System.out.println("あなたの勝ちです");
    }

    public void printYouLose() {
        System.out.println("あなたの負けです");
    }

    public void printDraw() {
        System.out.println("引き分けです");
    }

    public void yesOrNo() {
        System.out.println("0:yes 1:no");
    }

    public void askHit() {
        System.out.println("カードを引きますか？");
    }
}
