package com.example;

import java.util.List;

public interface Player {

    void hit(Card card);

    void stand();

    List<Card> showHands();

    int getTotalScore();
}
